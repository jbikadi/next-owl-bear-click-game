import { Actions } from "@/pages";

export const skills = (
    attack: number,
    autoAttack: number,
    location: number
) => [
    {
        name: 'Attack',
        type: Actions.AttackUp,
        cost: 3,
        level: attack
    },{
        name: 'Auto Attack',
        type: Actions.AutoAttackUp,
        cost: 3,
        level: autoAttack
    },{
        name: 'Location',
        type: Actions.Location,
        cost: 200,
        level: location
    }
];

export const items = () => [
    {
        name: 'Blunted Bronze Sword',
        type: Actions.AttackItems,
        cost: 50,
        required: 10
    },{
        name: 'Rough Grinding Stone',
        type: Actions.AttackItems,
        cost: 50,
        required: 20
    }
];

export const autoTraining = () => [
    {
        name: 'Wooden Training Sword',
        type: Actions.AutoAttackTraining,
        cost: 50,
        required: 10
    },{
        name: 'Skipping Rope',
        type: Actions.AutoAttackTraining,
        cost: 50,
        required: 20
    }
];