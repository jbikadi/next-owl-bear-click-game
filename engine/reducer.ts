import { State } from '../context/GameState';
import { Actions } from '../pages/';

export interface ReducerActions {
    type: Actions;
    payload?: {
        name: string;
    } | {
        locations: number;
    } | {
        name: string;
        maxHp: number;
        xp: number;
    }[];
}

export function xpCostHelper(stat:number, modifier:number) {
    const x = (stat === 0) ? 1 : stat;
    return x * modifier > 10 ? x * modifier : 10
}

function getGold({opponent, location, gold}: State) {
    if (opponent.xp === 0) {
        return gold;
    }
    const min = (opponent.xp - location < 1) ? 1 : opponent.xp - location;
    const max = opponent.xp + location;
    return gold + Math.floor(Math.random() * (max - min + 1) + min);
}

function opponent(state: State) {
    const random = Math.floor(Math.random() * state.opponents.length);
    const xp = state.xp + state.opponent.xp;
    const gold = getGold(state);
    return {
        ...state,
        opponent: state.opponents[random],
        hp: state.opponents[random].maxHp,
        xp,
        gold
    }
}

export function reducer(state: State, action:ReducerActions) {
    const payload =  action.payload || {};
    const exp = state.xp - state.xpSpent;
    let upgrade;

    switch (action.type) {
        case Actions.Attack:
            if (state.hp < 1) {
                return opponent(state);
            } else {
                return {...state, hp: state.hp - state.attack * state.attackLevel};
            }
        case Actions.AutoAttack:
            if (state.hp < 1) {
                return opponent(state);
            } else {
                return {...state, hp: state.hp - state.autoAttack * state.autoAttackLevel};
            }
        case Actions.Opponent:
            return opponent(state);
        case Actions.Opponents:
            if (Array.isArray(payload)) {
                return {...state, opponents: payload}
            }
        case Actions.AttackUp:
            // xp modifier = 3;
            upgrade = xpCostHelper(state.attack, 3);
            if (exp >= upgrade) {
                return {
                    ...state,
                    attack: state.attack + 1,
                    xpSpent: state.xpSpent + upgrade
                };
            }
        case Actions.AutoAttackUp:
            // xp modifier 3
            upgrade = xpCostHelper(state.autoAttack, 3);
            if (exp >= upgrade) {
                return {
                    ...state, 
                    autoAttack: state.autoAttack + 1,
                    xpSpent: state.xpSpent + upgrade,
                    delay: 1000
                };
            }
        case Actions.Location:
            // xp modifier 200
            upgrade = xpCostHelper(state.location, 200);
            if (exp >= upgrade && state.location < state.locationMax) {
                return {
                    ...state,
                    location: state.location + 1,
                    xpSpent: state.xpSpent + upgrade
                };
            }
        case Actions.LocationMax:
            if ('locations' in payload && typeof payload.locations === 'number') {
                return {...state, locationMax: payload.locations};
            }
        case Actions.AttackItems:
            // xp modifier 50
            upgrade = xpCostHelper(state.equipment.length + 1, 50);
            if ('name' in payload && typeof payload.name === 'string' && state.gold >= upgrade) {
                return {
                    ...state,
                    equipment: [...state.equipment, payload.name],
                    attackLevel: state.attackLevel + 1,
                    gold: state.gold - upgrade
                };
            }
        case Actions.AutoAttackTraining:
            // xp modifier 50
            upgrade = xpCostHelper(state.training.length + 1, 50);
            if ('name' in payload && typeof payload.name === 'string' && state.gold >= upgrade) {
                return {
                    ...state,
                    training: [...state.training, payload.name],
                    autoAttackLevel: state.autoAttackLevel + 1,
                    gold: state.gold - upgrade
                };
            }
        default:
            return state;
    }
}