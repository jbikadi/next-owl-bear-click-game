import { Actions } from "@/pages";
import { Dispatch, useEffect } from "react";
import { ReducerActions } from "./reducer";

export function useLocation(dispatch: Dispatch<ReducerActions>, location: number) {
    useEffect(() => {
        fetch(`/api/opponents/${location - 1}`)
        .then((response) => response.json())
        .then(json => dispatch({type: Actions.Opponents, payload: json}));
    }, [location, dispatch]);
}

export function useLocationMax(dispatch: Dispatch<ReducerActions>) {
    useEffect(() => {
        fetch('/api/opponents')
            .then(res => res.json())
            .then(json => dispatch({type: Actions.LocationMax, payload: json}))
    }, [dispatch]);
}