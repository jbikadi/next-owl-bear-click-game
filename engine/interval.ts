import { useEffect, useRef } from "react";

export function useInterval(callback: () => void, delay: number | null) {
    const savedCallBack = useRef<() => void>();

    useEffect(() => {
        savedCallBack.current = callback;
    });

    useEffect(() => {
        function tick() {
            if ('current' in savedCallBack && typeof savedCallBack.current !== 'undefined') {
                savedCallBack.current();
            }
        }

        if (delay !== null) {
            let id = setInterval(tick, delay);
            return () => clearInterval(id);
        }
    }, [delay]);
}