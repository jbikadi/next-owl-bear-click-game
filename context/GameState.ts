
export interface State {
    attack: number;
    attackLevel: number;
    autoAttack: number;
    autoAttackLevel: number;
    equipment: string[];
    training: string[];
    hp: number;
    location: number;
    locationMax: number;
    xp: number;
    xpSpent: number;
    gold: number;
    delay: number | null;
    opponent: {
        name: string;
        maxHp: number;
        xp: number;
    };
    opponents: {
        name: string;
        maxHp: number;
        xp: number;
    }[];
}

export const DEFAULT_STATE = {
    attack: 1,
    autoAttack: 0,
    attackLevel: 1,
    autoAttackLevel: 1,
    equipment: [],
    training: [],
    hp: 0,
    location: 1,
    locationMax: 0,
    xp: 0,
    xpSpent: 0,
    gold: 0,
    delay: null,
    opponent: {
        name: '',
        maxHp: 0,
        xp: 0
    },
    opponents: []
}