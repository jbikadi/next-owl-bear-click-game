import { Dispatch } from "react";
import { items, autoTraining } from "../../engine/UpgradesList";
import { ReducerActions, xpCostHelper } from '@/engine/reducer';
import { Button } from '../ui/_button';
import { State } from "@/context/GameState";
import { Actions } from "..";

interface ShopState {
    state: State;
    dispatch: Dispatch<ReducerActions>;
}

type LoopKind = {
    loop: {
        name: string;
        type: Actions;
        cost: number;
        required: number;
    }[],
    level: number;
    gold: number;
    list: string[];
    dispatch: Dispatch<ReducerActions>;
}

function ListedButtons({loop, level, gold, list, dispatch}: LoopKind) {
    const lineItem = loop.filter(({name}, i) => !list.includes(name) && list.length + 1 !== i)
        .map(({name, type, cost, required}, i) => {
            const listLevel = list.length + 1;
            const xpCost = xpCostHelper(listLevel, cost);
            const disabled = gold < xpCost || level < required;

            return (
                <div key={i} className='grid grid-cols-2 gap-4 mb-2'>
                    <div>
                        <div>Price: {xpCost} gold</div>
                        <div>Level: {level}/{required}</div>
                    </div>
                    <Button
                        name={name}
                        disabled={disabled}
                        clickHandler={() => dispatch({type: type, payload: {name: name}})}
                    />
                </div>
            )
        }
    );

    return (
        <>{(lineItem.length > 0) ? lineItem : <>sold out</>}</>
    );
}

export default function Shop({state: {attack, autoAttack, gold, equipment, training}, dispatch}:ShopState) {
    const itemsArray = items();
    const trainingArray = autoTraining();

    return (
        <section className="flex-auto basis-full sm:basis-2/5 xl:basis-1/5 p-4 border rounded">
            <h2>Shop:</h2>
            <div>
                <ListedButtons
                    loop={itemsArray}
                    level={attack}
                    gold={gold}
                    list={equipment}
                    dispatch={({type, payload}) => dispatch({type, payload})}
                />
            </div>
            <div>
                <ListedButtons
                    loop={trainingArray}
                    level={autoAttack}
                    gold={gold}
                    list={training}
                    dispatch={({type, payload}) => dispatch({type, payload})}
                />
            </div>
        </section>
    );
}