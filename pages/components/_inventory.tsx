import { State } from "@/context/GameState";

interface InventoryState {
    state: State;
}

export default function Inventory({state: {gold, equipment, training}}:InventoryState) {
    return (
        <section className="flex-1 basis-full sm:basis-2/5 xl:basis-1/5 p-4 border rounded">
            <h2>Inventory:</h2>
            <div className="mb-2">Gold: {gold}</div>
            {equipment.length > 0 &&
                <div>
                    <h3 className="text-lg">Battle gear:</h3>
                    <ul className="mb-4 list-dash">
                        {equipment.map((name) => (<li key={name} className="ml-4">{name}</li>))}
                    </ul>
                </div>
            }
            {training.length > 0 &&
                <div>
                    <h3 className="text-lg">Training gear:</h3>
                    <ul className="list-dash">
                        {training.map((name) => (<li key={name} className="ml-4">{name}</li>))}
                    </ul>
                </div>
            }
        </section>
    )
}