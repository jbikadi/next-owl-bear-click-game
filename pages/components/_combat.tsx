import { Dispatch } from "react";
import { Actions } from "..";
import { ReducerActions } from "@/engine/reducer";
import { Button } from "../ui/_button";

type Opponent = {
    opponent: {
        name: string;
        maxHp: number;
    };
    hp: number;
    clickHandler: Dispatch<ReducerActions>;
}

export default function Combat({opponent: {name, maxHp}, hp, clickHandler}: Opponent) {
    const btnLabel: string = (hp < 1) ? 'Next Opponent' : 'Attack';
    return (
        <section className="basis-full flex flex-col items-center p-4 border rounded">
            <h2 className="mb-4 text-xl">{name}</h2>
            <div className="mb-4 text-l">{hp} / {maxHp}</div>
            <div className="pb-1 w-[15rem]"><Button name={btnLabel} clickHandler={() => clickHandler({type: Actions.Attack})} /></div>
        </section>
    )
}