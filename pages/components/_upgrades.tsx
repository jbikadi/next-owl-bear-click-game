import { Dispatch } from 'react';
import { skills } from "../../engine/UpgradesList";
import { Actions } from "..";
import { ReducerActions, xpCostHelper } from '@/engine/reducer';
import { State } from "@/context/GameState";
import { Button } from '../ui/_button';

type UpgradeState = {
    state: State;
    dispatch: Dispatch<ReducerActions>;
}

export default function Upgrades({state: {attack, autoAttack, location, locationMax, xp, xpSpent}, dispatch}: UpgradeState) {
    const exp = xp - xpSpent;
    const skillsArray = skills(attack, autoAttack, location);

    return (
        <section className='flex-1 basis-full sm:basis-2/5 xl:basis-1/5 p-4 border rounded'>
            <h2>Levels:</h2>

            <div>
                {skillsArray.filter(({type, level}) => !(type === Actions.Location && level >= locationMax))
                .map( ({name, type, cost, level}, i) => {
                    const xpCost = xpCostHelper(level, cost);
                    const disabled = exp < xpCost;
                    return (
                        <div key={i} className='grid grid-cols-2 gap-4 mb-2'>
                            <div>Required: {xpCost}xp</div>
                            <Button
                                name={name}
                                disabled={disabled}
                                clickHandler={() => dispatch({type: type})}
                            />
                        </div>
                    );
                })}
            </div>
        </section>
    )
}