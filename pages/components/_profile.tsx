import { State } from "@/context/GameState";

// component to display players current stats and items
type stats = {
    state: State;
}

export default function Profile({state: {attack, autoAttack, xp, xpSpent, equipment, training}}: stats) {
    return (
        <section className="flex-1 basis-full sm:basis-2/5 xl:basis-1/5 p-4 border rounded grid grid-cols-fit gap-x-4">
            <h2 className="col-span-full">Stats:</h2> 
            <div>Attack:</div>
            <div>{attack * (equipment.length + 1)}</div>
            <div>Auto Attack:</div>
            <div>{autoAttack * (training.length + 1)}</div>
            <div>Experience:</div>
            <div>{xp - xpSpent}</div>
        </section>
    )
}