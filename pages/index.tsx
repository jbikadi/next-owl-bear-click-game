import { useState, useReducer } from 'react'
import { DEFAULT_STATE } from '@/context/GameState'
import { reducer } from '@/engine/reducer'
import { useInterval } from '@/engine/interval'
import { useLocation, useLocationMax } from '@/engine/useLocation'

import Combat from './components/_combat'
import Profile from './components/_profile'
import Upgrades from './components/_upgrades'
import Inventory from './components/_inventory'
import Shop from './components/_shop'
import { Button } from './ui/_button'

export enum Actions {
    Attack,
    AutoAttack,
    AttackUp,
    AutoAttackUp,
    Location,
    LocationMax,
    AttackItems,
    AutoAttackTraining,
    Opponent,
    Opponents,
}

export default function Home() {
    const [newGame, setNewGame] = useState(false);
    const [state, dispatch] = useReducer(reducer, DEFAULT_STATE);
    const disabled = state.opponents.length == 0;

    // Fetches an array of objects from the local api
    useLocation(dispatch, state.location);

    // Sets a cap on location upgrades to disable the button
    useLocationMax(dispatch);

    // initiate auto attack
    useInterval(() => {
        dispatch({type: Actions.AutoAttack});
    }, state.delay);

    if (newGame) {
        return (
            <main className='flex flex-wrap gap-4 mx-4 pt-4'>
                <Combat opponent={state.opponent} hp={state.hp} clickHandler={({type}) => dispatch({type})} />
                <Profile state={state} />
                <Upgrades state={state} dispatch={({type, payload}) => dispatch({type, payload})} />
                <Shop state={state} dispatch={({type, payload}) => dispatch({type, payload})} />
                <Inventory state={state} />
            </main>
        );
    } else {
        return (
            <main className='flex justify-center pt-4 mx-4'>
                <div>
                    <Button name={'New Game'} disabled={disabled} clickHandler={() => {
                        dispatch({type: Actions.Opponent});
                        setNewGame(() => !newGame);
                    }} />
                </div>
            </main>
        );
    }
}