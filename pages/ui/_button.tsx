import { MouseEventHandler } from "react";

interface ButtonKind {
    name: string;
    disabled?: boolean;
    clickHandler: MouseEventHandler<HTMLButtonElement>;
}

// generic button used in the combat and upgrades disabled value is optional
// example: <Button name={buttonname} disabled={true} onClick={() => onClick()} />
export function Button({name, disabled, clickHandler}: ButtonKind) {
    const isDisabled = disabled || false;
    return (
        <button
            name={name}
            className="rounded border px-3 pb-1 w-full leading-none text-white bg-blue-600 hover:bg-blue-700 disabled:bg-black disabled:text-gray-300"
            disabled={isDisabled}
            onClick={(e) => clickHandler(e)}
        >{name}</button>
    );
}