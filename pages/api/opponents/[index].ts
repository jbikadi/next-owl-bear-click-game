import type { NextApiRequest, NextApiResponse } from "next";
import opponents from "../../../context/oponents.json";

type data = {
    name: string;
    maxHp: number;
    xp: number;
}[];

const handler = (req: NextApiRequest, res: NextApiResponse<data>) => {
    const index = parseInt(req.query.index as string);
    res.status(200).json(opponents[index]);
};

export default handler;