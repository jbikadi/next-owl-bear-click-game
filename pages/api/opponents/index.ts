import type { NextApiRequest, NextApiResponse } from "next";
import opponents from "../../../context/oponents.json";

type data = {
    locations: number;
}

const handler = (req: NextApiRequest, res: NextApiResponse<data>) => {
    res.status(200).json({locations: opponents.length})
};

export default handler;